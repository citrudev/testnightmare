
var Nightmare = require('nightmare')
require('nightmare-xpath')
var apiKey = 'e665906f53484746b4b783862b0a643c'
var proxy = 'http://proxy.crawlera.com:8010/fetch?url='
var options = {
  show: false,
  maxAuthRetries: 10,
  switches: {
    'ignore-certificate-errors': true
  }

}



module.exports = function (app) {
  app.post('/', function (req, res) {
    var params = {}
    params.url = req.body.url
    params.keyword = req.body.keyword
    params.negKeyword = req.body.negKeyword
    params.Nightmare = Nightmare
    params.options = options
    params.proxy = proxy
    params.apiKey = apiKey

    fetch(params, function (err, result) {
      if (!err) {
        res.send(result)
      } else {
        res.send(err)
      }
    })
  })
}

function fetch (params, cb) {
  try {
    params.keyword = params.keyword.toString().trim()
    params.negKeyword = params.negKeyword.toString().trim()
    var keywordSelector = "//text()[matches(., '" + params.keyword + "', 'i')]"
    var negKeywordSelector = "//text()[matches(., '" + params.negKeyword + "', 'i')]"
    var nightmare = new Nightmare(params.options)
    nightmare.authentication(params.apiKey, '')
    .goto(params.proxy + '' + params.url)
    .inject('js', 'node_modules/jquery/dist/jquery.js')
    .inject('js', 'jquery.xpath.min.js')
    .evaluate(function (keyword, negkeyword) {
      var dataset = {}
      dataset.keywordSelector = keyword
      dataset.negkeyword = negkeyword
      dataset.keywordNodes = $(document).xpath(keyword)
      dataset.negKeywordNodes = $(document).xpath(negkeyword)
      return dataset
    }, keywordSelector, negKeywordSelector)
    .end()
    .then(function (result) {
      cb(null, result)
    })
    .catch(function (error) {
      cb(error)
    })
  } catch (e) {
    cb(e)
  }
}
